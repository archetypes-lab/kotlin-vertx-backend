package archetypes.app

import io.vertx.core.Vertx
import io.vertx.junit5.VertxExtension
import io.vertx.junit5.VertxTestContext
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.springframework.context.annotation.AnnotationConfigApplicationContext

@ExtendWith(VertxExtension::class)
class TestHttpVerticle {

  @BeforeEach
  fun deploy_verticle(vertx: Vertx, testContext: VertxTestContext) {

    // minimal spring IoC Container
    val spring = AnnotationConfigApplicationContext()
    spring.scan("archetypes.app")
    spring.refresh()

    // Exposition of the IoC Container
    SpringContext.set(spring)

    vertx.deployVerticle(HttpVerticle(), testContext.succeeding<String> { _ -> testContext.completeNow() })
  }

  @Test
  fun verticle_deployed(vertx: Vertx, testContext: VertxTestContext) {
    testContext.completeNow()
    Assertions.assertTrue(true)
  }
}
