package archetypes.app.http.handlers

import io.vertx.core.Vertx
import io.vertx.ext.web.Router


interface VertxHttpHandler {

  fun handle(vertx: Vertx, vararg  routers: Router)

}
