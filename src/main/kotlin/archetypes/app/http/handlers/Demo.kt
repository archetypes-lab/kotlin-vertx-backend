package archetypes.app.http.handlers

import archetypes.app.demo.DemoService
import io.vertx.core.Vertx
import io.vertx.core.http.HttpMethod
import io.vertx.ext.web.Router
import io.vertx.ext.web.RoutingContext
import io.vertx.kotlin.core.json.array
import io.vertx.kotlin.core.json.json
import io.vertx.kotlin.core.json.obj
import org.slf4j.LoggerFactory


class DemoHttpHandler(foo: String, demoService: DemoService) : VertxHttpHandler {

  private val log = LoggerFactory.getLogger(javaClass)

  private val foo: String
  private val demoService: DemoService

  init {
    this.foo = foo
    this.demoService = demoService
  }

  override fun handle(vertx: Vertx, vararg routers: Router) {
    val subRouter = Router.router(vertx)
    routers[0].mountSubRouter("/hello", subRouter)

    subRouter.route("/")
      .method(HttpMethod.GET)
      .handler(this::demoHandler)

    subRouter.route("/person/:personName")
      .method(HttpMethod.GET)
      .coroutineHandler(this::personName)
  }

  private fun demoHandler(ctx: RoutingContext) {
    log.debug("--> demoHandler")

    // Get the address of the request
    val address = ctx.request().connection().remoteAddress().toString()

    // Get the query parameter "name"
    val queryParams = ctx.queryParams()
    val name = if (queryParams.contains("name")) queryParams.get("name") else "unknown"

    // Write a json response

    ctx.json(json {
      obj(
        "name" to name,
        "address" to address,
        "message" to String.format("Hello %s connected from %s !!", name, address),
        "foo" to foo,
        "nations" to array("Chile", "Peru", "Brazil")
      )
    })

  }

  private suspend fun personName(ctx: RoutingContext) {
    log.debug("--> personName")
    val personName = ctx.pathParam("personName")

    val result = this.demoService.greetings(personName)

    ctx.json(json {
      obj(
        "greetings" to String.format("Hello World %s", personName),
        "grettingsKotlin" to "Hello World From Kotlin $personName",
        "coroutine result" to result
      )
    })
  }

}
