package archetypes.app.config

import archetypes.app.demo.DemoService
import archetypes.app.http.handlers.DemoHttpHandler
import archetypes.app.http.handlers.VertxHttpHandler
import io.github.cdimascio.dotenv.Dotenv
import io.vertx.core.Vertx
import io.vertx.core.http.HttpMethod
import io.vertx.ext.web.Router
import io.vertx.ext.web.handler.CorsHandler
import io.vertx.ext.web.handler.SessionHandler
import io.vertx.ext.web.sstore.redis.RedisSessionStore
import io.vertx.redis.client.Redis
import org.slf4j.LoggerFactory
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration

@Configuration
open class Base {

  private val log = LoggerFactory.getLogger(javaClass)

  @Bean
  open fun foo(dotenv: Dotenv): String {
    return dotenv["FOO"]
  }

  @Bean
  open fun dotenv(): Dotenv {
    return Dotenv.configure().ignoreIfMissing().ignoreIfMalformed().load()
  }

  // http

  @Bean
  open fun corsOriginPattern(dotenv: Dotenv): String {
    return dotenv["CORS_ORIGIN_PATTERN"]
  }

  @Bean
  open fun vertx(): Vertx {
    return Vertx.vertx()
  }

  @Bean
  open fun sessionClient(vertx: Vertx, dotenv: Dotenv): Redis {
    val redisSessionConString = dotenv["SESSION_REDIS_CONNECTION"]
    log.info("===> SESSION_REDIS_CONNECTION=$redisSessionConString")
    return Redis.createClient(vertx, redisSessionConString)
  }

  @Bean
  open fun mainRouter(vertx: Vertx, corsOriginPattern: String, sessionClient: Redis): Router {
    val mainRouter = Router.router(vertx)

    // cors
    mainRouter.route().handler(
      CorsHandler.create(corsOriginPattern)
        .allowCredentials(true)
        .allowedHeaders(
          setOf(
            "Access-Control-Request-Method", "Access-Control-Allow-Credentials",
            "Access-Control-Allow-Origin", "Access-Control-Allow-Headers", "Content-Type",
            "origin", "accept", "x-requested-with"
          )
        )
        .allowedMethods(
          setOf(
            HttpMethod.GET, HttpMethod.POST, HttpMethod.PUT,
            HttpMethod.DELETE, HttpMethod.OPTIONS, HttpMethod.PATCH
          )
        )
    )

    // redis http session
    val sessionStore = RedisSessionStore.create(vertx, sessionClient)
    val sessionHandler = SessionHandler.create(sessionStore)
    mainRouter.route().handler(sessionHandler)

    return mainRouter
  }

  @Bean
  open fun routerV1(vertx: Vertx, mainRouter: Router): Router {
    val routerV1 = Router.router(vertx)
    mainRouter.mountSubRouter("/api/v1", routerV1)
    return routerV1
  }

  // handlers

  @Bean
  open fun demoHttpHandler(vertx: Vertx, routerV1: Router, foo: String, demoService: DemoService): VertxHttpHandler {
    val handler = DemoHttpHandler(foo, demoService)
    handler.handle(vertx, routerV1)
    return handler
  }


}
