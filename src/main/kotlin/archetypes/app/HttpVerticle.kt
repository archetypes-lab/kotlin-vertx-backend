package archetypes.app

import io.vertx.ext.web.Router
import io.vertx.kotlin.coroutines.CoroutineVerticle
import io.vertx.kotlin.coroutines.await
import org.slf4j.LoggerFactory

class HttpVerticle : CoroutineVerticle() {

  private val log = LoggerFactory.getLogger(javaClass)

  override suspend fun start() {

    log.info("starting new instance of HttpVerticle")

    val router = SpringContext.getBean("mainRouter", Router::class.java)

    val server = vertx.createHttpServer()
    try {
      server
        .requestHandler(router)
        .listen(8888)
        .await()
      log.info("HTTP server started on port ${server.actualPort()}")
    } catch (e: Exception) {
      log.error("HTTP server not started!")
      throw e
    }

  }

}
