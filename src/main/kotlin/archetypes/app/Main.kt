package archetypes.app

import io.vertx.core.DeploymentOptions
import io.vertx.core.Vertx
import org.slf4j.LoggerFactory
import org.springframework.context.annotation.AnnotationConfigApplicationContext

private val log = LoggerFactory.getLogger("main")

fun main(args: Array<String>) {

  log.info("Kotlin application start!!!")

  // minimal spring IoC Container
  val spring = AnnotationConfigApplicationContext()
  spring.scan("archetypes.app")
  spring.refresh()

  // Exposition of the IoC Container
  SpringContext.set(spring)

  val vertx = SpringContext.getBean(Vertx::class.java)

  vertx.deployVerticle(
    HttpVerticle::class.qualifiedName,
    DeploymentOptions().setInstances(Runtime.getRuntime().availableProcessors() * 2)
  )

}
