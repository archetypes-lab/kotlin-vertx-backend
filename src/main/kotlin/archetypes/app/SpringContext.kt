package archetypes.app

import org.springframework.context.ApplicationContext

class SpringContext {

  companion object {
    private var context: ApplicationContext? = null

    fun set(context: ApplicationContext) {
      this.context = context
    }

    fun <T> getBean(name: String, requiredType: Class<T>): T {
      return this.context!!.getBean(name, requiredType)
    }

    fun <T> getBean(requiredType: Class<T>): T {
      return this.context!!.getBean(requiredType)
    }

  }

}
