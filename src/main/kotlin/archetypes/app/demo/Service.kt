package archetypes.app.demo

import kotlinx.coroutines.delay

interface DemoService {
  suspend fun greetings(name: String) : String
}

class DefaultDemoService : DemoService {
  override suspend fun greetings(name: String): String {
    delay(10000)
    return "hello $name from coroutine"
  }
}

